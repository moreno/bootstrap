#!/bin/bash
MYNAME="moreno"
ZSHTHEME="robbyrussel"
MYSHELL="zsh"


# some essentials 
sudo apt update && sudo apt install -y vim-nox tmux wget curl git zsh htop openssh-server


# install oh-my-zsh framework
git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
sudo chsh -s $(which $MYSHELL) $USER


GIT_CONFIG_FILE=~/.gitconfig
touch $GIT_CONFIG_FILE
echo "[user]" > $GIT_CONFIG_FILE
echo "name = $MYNAME" >> $GIT_CONFIG_FILE
 

